Superflow
=============
A SusyNtuple looper for producing flat ntuples. 
Faciliates adding selections, variables, and systematic variations.

# Pre-Requisites
Setup the SusyNt packages needed to read SusyNtuples (see [UCINtSetup](https://gitlab.cern.ch/alarmstr/UCINtSetup))

# Setup Superflow package
It is assumed that the top level directory structure for the project contains a `source\`, `build\`, and `run\` directory as discussed in the [ATLAS Software Tutorial ](https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/).
From within this top level directory, do the following things
```bash
cd source
git clone ssh://git@gitlab.cern.ch:7999/susynt/superflow.git
cd ..
```

# Using Superflow

## Test run

Run `SuperflowAna -h` to see executable options
```bash
SuperflowAna -i path/to/SusyNt_file.root -c
```

## Making your own superflow looper

TODO

